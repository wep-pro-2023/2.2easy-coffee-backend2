import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { OrdersService } from './orders.service';
import { CreateOrderDto } from './dto/create-oder.dto';
import { UpdateOrderDto } from './dto/update-oder.dto';

@Controller('orders')
export class OrdersController {
  constructor(private readonly odersService: OrdersService) {}

  @Post()
  create(@Body() createOrderDto: CreateOrderDto) {
    return this.odersService.create(createOrderDto);
  }

  @Get()
  findAll() {
    return this.odersService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.odersService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateOrderDto: UpdateOrderDto) {
    return this.odersService.update(+id, updateOrderDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.odersService.remove(+id);
  }
}
